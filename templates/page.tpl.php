<?php // $Id: page.tpl.php,v 1.1.2.5.2.14.2.12 2010/03/01 13:37:46 psynaptic Exp $ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html<?php print drupal_attributes($html_attr); ?>>

<head>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if lt IE 8]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_theme; ?>css/ie-lt8.css" /><![endif]-->
  <?php print $scripts; ?>
  <title><?php print $head_title; ?></title>
</head>

<body<?php print drupal_attributes($attr); ?>>

  <div id="body-wrapper">
	
	<?php if ($navigation): ?>
	  <div id="navigation">
	    <div class="limiter clear-block">
	      <?php print $skip_link; ?>
	      <?php print $navigation; ?>     
	    </div>
	  </div>
  <?php endif; ?>
  
	<div id="branding">
    <div class="limiter clear-block">
      <?php if ($logo_themed): ?>
				<div id="logo">
					<?php print $logo_themed; ?>
				</div>
			<?php endif; ?>
      
			<?php if ($site_name_themed || $site_slogan_themed): ?>
				<?php if ($site_name_themed): ?>
					<div id="site-name-themed">
						<?php print $site_name_themed; ?>
					</div>					
      	<?php endif; ?>

				<?php if ($site_slogan_themed): ?>
					<div id="site-slogan-themed">
						<?php print $site_slogan_themed; ?>
					</div>
				<?php endif; ?>
      <?php endif; ?>
			
			<?php if ($mission_themed): ?>
				<div id="mission-themed">
					<?php print $mission_themed; ?>
				</div>
      <?php endif; ?>
			
			<?php if ($search_box): ?>
				<div id="search">
					<?php print $search_box; ?>
				</div>
    	<?php endif; ?>

    </div>
  </div>
 

  <div id="page">
    <div class="limiter clear-block">
      <div id="main" class="clear-block">

        <?php if ($left): ?>
          <div id="left" class="sidebar">
            <?php print $left; ?>
          </div>
        <?php endif; ?>

        <div id="content" class="clear-block">
				
					<?php if ($messages): ?>
						<div id="messages">
			    		<?php print $messages; ?>
			  		</div>
          <?php endif; ?>

					<?php print $tabs; ?>
          
          <?php if ($title): ?>
            <h1 class="page-title"><?php print $title; ?></h1>
          <?php endif; ?>
          
          <?php print $help; ?>
          
          <div id="below-title">
            <?php print $content; ?>
          </div>
        </div>

        <?php if ($right): ?>
          <div id="right" class="sidebar">
            <?php print $right; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
	
	<?php if ($footer): ?>
  	<div id="footer">
	    <div class="limiter clear-block">
				<?php print $footer; ?>
	    </div>
	  </div>
	<?php endif; ?>
	
  <?php print $closure; ?>

<!-- body wrapper -->
</div>

</body>
</html>
